import React from 'react';
import InputField from './InputField';
import Leaderboard from './Leaderboard';

interface Props {
    difficulty: number;
    guess: string;
    setGuess: (guess: string) => void;
}

interface State {
    answer: number;
    guesses: number;
    hint: string;
    name: string;
    scores: { name: string, score: number }[];
    started: boolean;
}

export default class GuessingGame extends React.PureComponent<Props, State> {
    public constructor(props: Props) {
        super(props); 

        this.state = {
            answer: Math.floor(Math.random() * 100),
            guesses: 0,
            hint: '',
            name: '',
            scores: [ { name: 'SK', score: 1337 }, { name: 'Love', score: 666 } ],
            started: false,
        }
    }

    private onChangeName = (name: string) => {
        this.setState({ name });
    }

    private startGame = () => {
        this.setState({ started: true });
    }
    
    private makeGuess = () => {
        this.setState({ guesses: this.state.guesses + 1 });

        if (parseInt(this.props.guess) === this.state.answer) {
            this.setState( { 
                answer: Math.floor(Math.random() * 100),
                guesses: 0,
                hint: '',
                scores: this.state.scores.concat({ name: this.state.name, score: this.state.guesses + 1 }),
            });
        } else if (parseInt(this.props.guess) > this.state.answer) {
            this.setState({ hint: 'Try lower!' });
        } else {
            this.setState({ hint: 'Try higher!' });
        }

        this.props.setGuess('');
    }

    public render() {
        return (
            <div className="container-md mt-2">
                <div>
                    <h1>Guessing Game</h1>
                    <p>Guess a number between 0-{ this.props.difficulty }</p>
                </div>

                { !this.state.started && (
                    <div>
                        <InputField label="Enter your name" name="name" value={ this.state.name } onChange={ this.onChangeName } />
                        <button className="btn btn-primary" onClick={ this.startGame }>Start!</button>
                    </div>
                ) }

                { this.state.started && (
                    <div>
                        <p>Total guesses: { this.state.guesses }</p>
                        { this.state.hint && <p>{ this.state.hint }</p> }
                        <InputField label="Guess" name="guess" value={ this.props.guess } onChange={ this.props.setGuess } />
                        <button className="btn btn-primary" onClick={ this.makeGuess }>Guess!</button>
                    </div>
                ) }

                <Leaderboard scores={ this.state.scores } />
            </div>
        );
    }
}
