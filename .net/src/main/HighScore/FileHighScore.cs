﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GuessingGame.HighScore
{
    public class FileHighScore
    {

        private readonly string highScoreFile;

        public FileHighScore() : this("\\main\\HighScore\\file\\highscore.txt")
        {
        }

        public FileHighScore(string highScoreFile)
        {
            var path = Directory.GetParent(Directory.GetCurrentDirectory())?.Parent?.Parent?.Parent?.FullName + highScoreFile;

            this.highScoreFile = path;
        }

        public void AddScore(string player, int numGuesses)
        {
            List<Score> scores = Load();
            Score existingScore = GetScoreForPlayer(player);
            if (existingScore == null)
            {
                scores.Add(new Score(player, numGuesses));
                store(scores);
            }
            else if (numGuesses < existingScore.NumGuesses)
            {
                scores.RemoveAll(score => score.Player.Equals(player));
                scores.Add(new Score(player, numGuesses));
                store(scores);
            }
        }

        public List<Score> Scores
        {
            get { return Load(); }
        }

        public void ClearScores()
        {
            Clear();
        }

        private Score GetScoreForPlayer(string player)
        {
            foreach (Score score in Scores)
            {
                if (score.Player.Equals(player))
                {
                    return score;
                }
            }

            return null;
        }

        private List<Score> Load()
        {
            // load scores from file
            List<Score> scores = new List<Score>();
            try
            {
                if (File.Exists(highScoreFile))
                {
                    using (var sr = File.OpenText(highScoreFile))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            scores.Add(ParseLine(s));
                        }

                        return scores;
                    }
                }
                var file = new FileInfo(highScoreFile);
                file.Directory?.Create();
                File.WriteAllText(file.FullName, null);

                return scores;
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
                throw new Exception("No highscore file");
            }
        }

        private Score ParseLine(string line)
        {
            // parse score from line
            string[] nameAndGuesses = line.Split(":");
            return new Score(nameAndGuesses[0], int.Parse(nameAndGuesses[1]));
        }

        private void store(List<Score> scores)
        {
            FileInfo fi = new FileInfo(highScoreFile);

            // save scores to file     
            var sw = fi.CreateText();
            var output = string.Join(Environment.NewLine,
                scores.Select(score => string.Format("{0}:{1:D}", score.Player, score.NumGuesses)));
            sw.Write(output);
            sw.Flush();
            sw.Close();
        }

        private void Clear()
        {
            File.WriteAllText(highScoreFile, string.Empty);
        }
    }
}

