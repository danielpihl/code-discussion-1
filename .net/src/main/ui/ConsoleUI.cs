﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GuessingGame.HighScore;

namespace GuessingGame.ui
{
    public class ConsoleUi
    {
        public string AskForPlayerName()
        {
            Console.WriteLine("Enter player name: ");
            var player = Console.ReadLine();
            Console.WriteLine(string.Format("Welcome {0}", player));
            return player;
        }

        public void ShowInstructions()
        {
            Console.WriteLine("\n--- Welcome to Guessing Game ---");
            Console.WriteLine("\nI am thinking of a number between 0 and 1000, your job is to guess the number!");
        }

        public int AskForNumberFromPlayer()
        {
            Console.WriteLine("What is your guess? ");
            var input = Convert.ToInt32(Console.ReadLine());

            return input;
        }

        public void PromptHigher()
        {
            Console.WriteLine("Incorrect, my number is higher.");
        }

        public void PromptLower()
        {
            Console.WriteLine("Incorrect, my number is lower.");
        }

        public void Correct()
        {
            Console.WriteLine("Congratulations, you guessed it!");
        }

        public void ShowHighScores(List<Score> scores)
        {
            Console.WriteLine(string.Format("\n{0,25}", "--- Highscore ---"));
            Console.WriteLine(string.Format("Pos {0,15} {1,15}", "Player", "Guesses"));
            StringBuilder b = new StringBuilder();
            scores = scores.OrderBy(s => s.NumGuesses).ToList();

            for (var i = 0; i < scores.Count; i++)
            {
                Score score = scores[i];
                b.Append(string.Format("{0:D}, {1,15} {2,15:D} \n", i + 1, score.Player, score.NumGuesses));
            }

            Console.WriteLine(b);
        }

        public bool AskForAnotherGame()
        {
            Console.WriteLine("Play again? (Y/N): ");
            var yesOrNo = Console.ReadLine();
            return yesOrNo.Equals("Y");
        }

        public void ShowGuesses(List<int> guesses)
        {
            Console.WriteLine("Num guesses = " + guesses.Count);
            Console.WriteLine("Your guesses = " + string.Join(",", guesses));
        }

        public void SayGoodbye()
        {
            Console.WriteLine("Thanks for playing!");
        }

        public bool ShouldClearScores()
        {
            Console.WriteLine("Clear scores? (Y/N)");
            var yesOrNo = Console.ReadLine();
            return yesOrNo.Equals("Y");
        }
    }
}